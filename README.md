# Octopus OKR Demo

Jupyter Notebook Demo of Octopus OKR study data visualization. Check it out on Binder!

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lamb%2Foctopus-okr-demo/HEAD?filepath=OctoAnalysisDemo.ipynb)

